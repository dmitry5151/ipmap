
export interface IpResult {
  status: string;
  country: string;
  countryCode: string;
  region: string;
  regionName: string;
  city: string;
  zip: string;
  lat: number;
  lon: number;
  timezone: string;
  isp: string;
  org: string;
  as: string;
  query: string;
}

export interface IpError {
  message: string;
  query: string;
  status: 'success' | 'fail';
}

export interface IpRequest {
  query: string;
  fields?: string;
  lang?: string;
}

export interface IpResultWithData extends IpResult {
  count: number;
}
