import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ipMock } from './api/mock';
import { ApiService } from './api/api.service';
import { IpResult, IpRequest, IpError, IpResultWithData } from '../../typings/ip-info-type';
import { Observable, ReplaySubject } from 'rxjs/Rx';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {

  @ViewChild('map') map: ElementRef;

  private ipData: IpResult;
  private requestBody: IpRequest[];
  private resultIPs: IpResultWithData[];
  // Если данные по ip придут раньше, чем инициализируется карта, они будут взяты из буфера
  private resultIPs$$: ReplaySubject<IpResultWithData[]> = new ReplaySubject(1);

  // Common
  private mockIp: any[] = ipMock;

  constructor(
    private service: ApiService,
  ) {
    const requestData = this.prepareRequestData();
    // Батч по 100 ip с задержкой 7ms на каждый следующий запрос
    const ipsInfo$ = requestData.map((request, i) => this.service.getIpData(request).pipe(delay(i * 7)));
    this.getResults(ipsInfo$);
  }

  ngOnInit() {
    this.initMap();
  }

  prepareRequestData(): Array<IpRequest[]> {
    const res: Array<IpRequest[]> = [];
    const ips: IpRequest[] = this.mockIp.map(ip => ({ query: ip[0], lang: 'ru' } as IpRequest));
    if (ips.length > 100) {
      const iterations = Math.ceil(ips.length / 100);
      for (let i = 0; i < iterations * 100; i += 100) {
        res.push(ips.slice(i, i + 100));
      }
    } else {
      res.push(ips);
    }
    return res;
  }

  getResults(ipsInfo$: Observable<IpResult[]>[]) {
    Observable.zip(...ipsInfo$).subscribe(result => {
      // Отфильтровать фейлы
      let resultIPs: IpResultWithData[] = [];
      result.forEach(ipData => resultIPs = resultIPs.concat(ipData as IpResultWithData[]));
      // Внедрить данные по кол-ву запросов на каждый ip
      resultIPs = resultIPs.map((resIP, i) => {
        resIP.count = this.mockIp[i][1];
        return resIP;
      }).filter((ipData: IpError | IpResult) => ipData.status === 'success');
      // Получили данные по ip, строим маркеры
      this.resultIPs$$.next(resultIPs);
    });
  }

  initMap() {
    const myLatlng = new google.maps.LatLng(27.086537, 2.936436);
    const options = {
      zoom: 3,
      center: myLatlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    const map = new google.maps.Map(this.map.nativeElement, options);
    // Subscribe to ip data
    this.resultIPs$$.subscribe(resultIPs => {
      this.resultIPs = resultIPs;
      this.setMarkers(map);
    });
  }

  setMarkers(map: any) {
    this.resultIPs.forEach(resIP => {
      const latlng = new google.maps.LatLng(resIP.lat, resIP.lon);
      const marker = new google.maps.Marker({
        position: latlng,
        map,
        title: `IP: ${resIP.query}; Запросов: ${resIP.count}`
      });
      // Set listeners & info windows
      const contentString = `<div id="content">
        <div>IP: <strong>${resIP.query}</strong></div>
        <div>Запросов: <strong>${resIP.count}</strong></div>
        </div>`;
      const infowindow = new google.maps.InfoWindow({
        content: contentString
      });
      google.maps.event.addListener(marker, 'click', () => {
        infowindow.open(map, marker);
      });
    });
  }
}
