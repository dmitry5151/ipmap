import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpEvent, HttpParams, HttpHeaders } from '@angular/common/http';
import { IpResult, IpRequest } from '../../../typings/ip-info-type.d';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError } from 'rxjs/operators';

const IP_BATCH_API = 'http://ip-api.com/batch';

@Injectable()
export class ApiService {

  /**
   * WARN: максимум 100 ip в одном запросе
   * Не более 150 запросов в минуту
   */
  private reqLimitIP = 100;
  private reqLimitPerSecond = 150;

  constructor(
    private http: HttpClient,
  ) { }

  getIpData(body: IpRequest[]): Observable<IpResult[]> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'text/plain',
      }),
    };
    return this.http.post<IpResult[]>(IP_BATCH_API, body, options)
      .pipe(
        catchError(this.handleError('getIpData', []))
      );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error);
      return of(result as T);
    };
  }

}
