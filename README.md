# IpMap. Отображение на карте мира местоположения IP адреса и кол-ва запросов, с него поступивших

## Запуск и установка

> Для установки потребуется: [Git](https://git-scm.com) и [Node.js](https://nodejs.org/en/) v. > 9.9.0

1. Клонировать репозиторий в локальную папку: `git clone git@gitlab.com:dmitry5151/ipmap.git`
2. Зайти в папку: `cd ipmap`
3. Выполнить установку зависимостей: `npm install`
4. Запустить проект локально: `npm start`
4. Открыть в браузере `http://localhost:4200`

---

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
